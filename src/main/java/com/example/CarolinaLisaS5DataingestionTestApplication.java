package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarolinaLisaS5DataingestionTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarolinaLisaS5DataingestionTestApplication.class, args);
	}
}
