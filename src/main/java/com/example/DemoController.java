package com.example;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@RequestMapping("/")
	public String helloworld(){
		return "Hello Class";
	}
	
	
	@RequestMapping("/name/{name}")
	public String helloworld (@PathVariable ("name") String name){
		return "Hello " + name + " !!!";
	}
}
